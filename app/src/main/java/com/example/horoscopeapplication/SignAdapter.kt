package com.example.horoscopeapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class SignAdapter(private val mainActivity: MainActivity, private val starSigns: List<String>):
    RecyclerView.Adapter<SignAdapter.ListItemHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListItemHolder {

        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)

        return ListItemHolder(itemView)
    }

    override fun getItemCount(): Int {
        //if (noteList != null) {
        return starSigns.size
        //}
        // error
        //return -1
    }

    override fun onBindViewHolder(
        holder: ListItemHolder, position: Int) {

        val sign = starSigns[position]
        holder.name.text = sign


    }

    inner class ListItemHolder(view: View) :
        RecyclerView.ViewHolder(view),
        View.OnClickListener {

        internal var name =
            view.findViewById<View>(
                R.id.signName) as TextView

        init {

            view.isClickable = true
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            mainActivity.displaySign(adapterPosition, name.text.toString())
        }



    }
}