package com.example.horoscopeapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.core.content.ContextCompat.startActivity
import android.content.Intent
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.util.Log
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject


class MainActivity : AppCompatActivity() {


    private var starSigns: ArrayList<String>? = null
    private var recyclerView: RecyclerView? = null
    private var adapter: SignAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        recyclerView =
            findViewById<View>(R.id.recyclerView) as RecyclerView
        starSigns = ArrayList()
        starSigns?.add("Aquarius")
        starSigns?.add("Pisces")
        starSigns?.add("Aries")
        starSigns?.add("Taurus")
        starSigns?.add("Gemini")
        starSigns?.add("Cancer")
        starSigns?.add("leo")
        starSigns?.add("Virgo")
        starSigns?.add("Libra")
        starSigns?.add("Scorpio")
        starSigns?.add("Sagittarius")
        starSigns?.add("Capricorn")


        adapter = SignAdapter(this, this.starSigns!!)
        val layoutManager = LinearLayoutManager(applicationContext)

        recyclerView!!.layoutManager = layoutManager

        recyclerView!!.adapter = adapter

        recyclerView!!.addItemDecoration(
            DividerItemDecoration(
                this, LinearLayoutManager.VERTICAL)
        )


    }


    fun displaySign(pos: Int, text: String){
        val myIntent = Intent(this, Sign::class.java)
        Log.d("Content", "$text")
        myIntent.putExtra("SIGN_NAME", text)
        startActivity(myIntent)

    }


}
