package com.example.horoscopeapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_sign.*
import org.json.JSONArray
import org.json.JSONObject

class Sign() : AppCompatActivity() {

    private var aries = SignInfo("Aries", "Courageous and Energetic.", "Ram", "April", "hi")
    private var taurus = SignInfo("Taurus", "Known for being reliable, practical, ambitious and sensual.", "Bull", "May", "")
    private var gemini = SignInfo("Gemini", "Gemini-born are clever and intellectual.", "Twins", "June", "")
    private var cancer = SignInfo("Cancer", "Tenacious, loyal and sympathetic.", "Crab", "July", "")
    private var leo = SignInfo("Leo", "Warm, action-oriented and driven by the desire to be loved and admired.", "Lion", "August", "")
    private var virgo = SignInfo("Virgo", "Methodical, meticulous, analytical and mentally astute.", "Virgin", "September", "")
    private var libra = SignInfo("Libra", "Librans are famous for maintaining balance and harmony.", "Scales", "October", "")
    private var scorpio = SignInfo("Scorpio", "Strong willed and mysterious.", "Scorpion", "November", "")
    private var sagittarius = SignInfo("Sagittarius", "Born adventurers.", "Archer", "December", "")
    private var capricorn = SignInfo("Capricorn", "The most determined sign in the Zodiac.", "Goat", "January", "")
    private var aquarius = SignInfo("Capricorn", "Humanitarians to the core.", "Water Bearer", "February", "")
    private var pisces = SignInfo("Pisces", "Proverbial dreamers of the Zodiac.", "Fish", "March", "")




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign)
        var signName = findViewById<TextView>(R.id.signName)
        var signDescription = findViewById<TextView>(R.id.signDescription)
        var signAnimal = findViewById<TextView>(R.id.signAnimal)
        var signMonth = findViewById<TextView>(R.id.signMonth)
        var signHoroscope = findViewById<TextView>(R.id.signHoroscope)
        val myIntent = Intent()
        val sign_name = intent.extras?.getString("SIGN_NAME")
        getHoroscope(sign_name)
        when(sign_name?.toLowerCase()){
            "aries" -> displayInfo(aries, signName, signDescription, signAnimal, signMonth, signHoroscope)
            "taurus" -> displayInfo(taurus, signName, signDescription, signAnimal, signMonth, signHoroscope)
            "gemini" -> displayInfo(gemini, signName, signDescription, signAnimal, signMonth, signHoroscope)
            "cancer" -> displayInfo(cancer, signName, signDescription, signAnimal, signMonth, signHoroscope)
            "leo" -> displayInfo(leo, signName, signDescription, signAnimal, signMonth, signHoroscope)
            "virgo" -> displayInfo(virgo, signName, signDescription, signAnimal, signMonth, signHoroscope)
            "libra" -> displayInfo(libra, signName, signDescription, signAnimal, signMonth, signHoroscope)
            "scorpio" -> displayInfo(scorpio, signName, signDescription, signAnimal, signMonth, signHoroscope)
            "sagittarius" -> displayInfo(sagittarius, signName, signDescription, signAnimal, signMonth, signHoroscope)
            "capricorn" -> displayInfo(capricorn, signName, signDescription, signAnimal, signMonth, signHoroscope)
            "aquarius" -> displayInfo(aquarius, signName, signDescription, signAnimal, signMonth, signHoroscope)
            "pisces" -> displayInfo(pisces, signName, signDescription, signAnimal, signMonth, signHoroscope)
        }
    }

    private fun displayInfo(sign: SignInfo, sName: TextView, sDesc: TextView, sAnimal: TextView, sMonth: TextView, sHoroscope: TextView){
        sName.text = sign.sign_name
        sDesc.text = sign.sign_description
        sAnimal.text = sign.sign_symbol
        sMonth.text =sign.sign_month

    }

    fun getHoroscope(name: String?){
        // Instantiate the RequestQueue.
        val queue = Volley.newRequestQueue(this)
        val url: String = "http://sandipbgt.com/theastrologer/api/horoscope/${name?.toLowerCase()}/today"
        //

        // Request a string response from the provided URL.
        val stringReq = StringRequest(Request.Method.GET, url,
            Response.Listener<String> { response ->
                var strResp = response.toString()
                val jsonObj: JSONObject = JSONObject(strResp)
                var dailyHoroscope = jsonObj.getString("horoscope")
                signHoroscope!!.text = "$dailyHoroscope"
            },
            Response.ErrorListener {signHoroscope!!.text = "That didn't work!"})
        queue.add(stringReq)
    }




}
